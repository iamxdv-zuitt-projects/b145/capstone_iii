// [SECTION] Dependencies and Modules
import express from 'express';
import colors from 'colors'; //adding colors in terminal to easily find errors
import dotenv from 'dotenv';

// [SECTION] PAGE ROUTE
import seedRouter from './routes/seedRoutes.js';
import productRouter from './routes/productRoutes.js';
import userRouter from './routes/userRoutes.js';
import orderRouter from './routes/orderRoutes.js';
import uploadRouter from './routes/uploadRoutes.js';
/*====================================================================================== */

// [SECTION] FILE ROUTE
// import data from './data.js';
// import path from 'path';
// import { fileURLToPath } from 'url';
// import { dirname } from 'path';
import connectDB from './mongoDB/mongoDB.js';
/*====================================================================================== */

// [SECTION] Server Setup
const app = express();
/*====================================================================================== */

//[SECTION] Environment Variables Setup
dotenv.config();
/*======================================================================================*/

//[SECTION] DATABASE CONNECT

connectDB();
/*======================================================================================*/

// [SECTION] Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 path.join returns a normalized path by merging two paths together. It can return an absolute path, but it doesn't necessarily always do so.

 For instance: path.join('app/libs/oauth', '/../ssl')
 resolves to app/libs/ssl

 path.resolve, on the other hand, will resolve to an absolute path.

For instance, when you run:

path.resolve('bar', '/foo');
The path returned will be /foo since that is the first absolute path that can be constructed.

for more info: https://stackoverflow.com/questions/39110801/path-join-vs-path-resolve-with-dirname
 */

// const __dirname = path.resolve();

/*  serve all files inside front end build folder as a static files.
So if you're port image scripts and HTML file inside this '/frontend/build' folder will be served by this server 'http://localhost:5000' */
// app.use(express.static(path.join(__dirname, '/frontend/build')));

/* everything that user enter after the website, domain or server name is going to be served by this 'index.html'
For now, we don't have this file because if we go to the front end folder, there is no build folder inside it to create build folder.
To Create build folder We need to run npm run build in the root folder because it goes to the front end folder and build the front end project.
 */
// app.get('*', (req, res) =>
//   res.sendFile(path.join(__dirname, '/frontend/build/index.html'))
// );
/*======================================================================================*/

//[SECTION] Server Routes
app.use('/api/upload', uploadRouter);
app.use('/api/seed', seedRouter);
app.use('/api/products', productRouter);
app.use('/api/users', userRouter);
app.use('/api/orders', orderRouter);

app.get('/api/keys/paypal', (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID || 'sb');
});
/*======================================================================================*/

//ERROR SERVER RESPONSE USING {Asyncerrorhandler}
app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});
/*====================================================================================== */

const port = process.env.PORT || 5000; //-> PORT >> .env

app.listen(port, () =>
  console.log(`Server is running at port ${port}`.yellow.bold)
);
