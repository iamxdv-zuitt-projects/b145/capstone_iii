import bcrypt from 'bcryptjs';

const data = {
  users: [
    {
      name: 'Admin User',
      email: 'admin@mail.com',
      password: bcrypt.hashSync('123456', 10),
      isAdmin: true,
    },
    {
      name: 'John Doe',
      email: 'john@mail.com',
      password: bcrypt.hashSync('123456', 10),
    },
    {
      name: 'Jane Doe',
      email: 'jane@mail.com',
      password: bcrypt.hashSync('123456', 10),
    },
  ],

  products: [
    {
      name: 'Airpods Wireless Bluetooth Headphones',
      image: '/images/airpods.jpg',
      description:
        'Bluetooth technology lets you connect it with compatible devices wirelessly High-quality AAC audio offers immersive listening experience Built-in microphone allows you to take calls while working',
      brand: 'Apple',
      category: 'Electronics',
      price: 4499.5,
      countInStock: 10,
      rating: 4.5,
      numReviews: 688,
      slug: 'Apple-Airpods',
    },
    {
      name: 'iPhone 11 Pro 256GB Memory',
      image: '/images/phone.jpg',
      description:
        'Introducing the iPhone 11 Pro. A transformative triple-camera system that adds tons of capability without complexity. An unprecedented leap in battery life',
      brand: 'Apple',
      category: 'Electronics',
      price: 30000.0,
      countInStock: 7,
      rating: 4.0,
      numReviews: 833,
      slug: 'Apple-iPhone_11-Pro_256G',
    },
    {
      name: 'Cannon EOS 80D DSLR Camera',
      image: '/images/camera.jpg',
      description:
        'Characterized by versatile imaging specs, the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design',
      brand: 'Cannon',
      category: 'Electronics',
      price: 46499.5,
      countInStock: 5,
      rating: 3,
      numReviews: 148,
      slug: 'Canon-EOS80D',
    },
    {
      name: 'Sony Playstation 4 Pro White Version',
      image: '/images/playstation.jpg',
      description:
        'The ultimate home entertainment center starts with PlayStation. Whether you are into gaming, HD movies, television, music',
      brand: 'Sony',
      category: 'Electronics',
      price: 19975.0,
      countInStock: 11,
      rating: 5,
      numReviews: 127,
      slug: 'Sony-PS4-Pro',
    },
    {
      name: 'Logitech G-Series Gaming Mouse',
      image: '/images/mouse.jpg',
      description:
        'Get a better handle on your games with this Logitech LIGHTSYNC gaming mouse. The six programmable buttons allow customization for a smooth playing experience',
      brand: 'Logitech',
      category: 'Electronics',
      price: 1990.0,
      countInStock: 7,
      rating: 3.5,
      numReviews: 100,
      slug: 'Logitect-G-Series-Gaming',
    },
    {
      name: 'Amazon Echo Dot 3rd Generation',
      image: '/images/alexa.jpg',
      description:
        'Meet Echo Dot - Our most popular smart speaker with a fabric design. It is our most compact smart speaker that fits perfectly into small space',
      brand: 'Amazon',
      category: 'Electronics',
      price: 1445.5,
      countInStock: 0,
      rating: 4,
      numReviews: 400,
      slug: 'Amazon-Echo-3rdGen',
    },
  ],
};

export default data;
